extends RigidBody

const pid = preload("pid.gd")
const grapple_scene = preload("res://scenes/player/Grapple.tscn")
const wire_scene = preload("res://scenes/player/wire.tscn")
const Kp = 0.5
const Ki = 0.1
const Kd = 6
var stand_up_x_pid = pid.new(Kp, Ki, Kd)
var stand_up_z_pid = pid.new(Kp, Ki, Kd)
const grapple_top_speed = 30
const grapple_accel = 4
var next_on_left_side = false
var grapple = null
var wire = null
var grapple_locked = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _input(event):
	if event.is_action_pressed("shoot_grapple"):
		if grapple and grapple_locked:
			grapple_locked = false
		else:
			grapple = grapple_scene.instance()
			var x = 0.25
			if next_on_left_side:
				x *= -1
			next_on_left_side = !next_on_left_side
			grapple.transform = transform * Transform(Basis(), Vector3(x, -0.2, -2.0))
			grapple.linear_velocity = linear_velocity
			grapple.angular_velocity = angular_velocity
			get_tree().get_root().add_child(grapple)
			point_wire_at_grapple()

#func _process(delta):
#	pass

func point_wire_at_grapple():
	if not grapple:
		return
	if not wire:
		wire = wire_scene.instance()
		add_child(wire)
	wire.global_transform = global_transform.looking_at(grapple.global_transform.origin, Vector3(0, 1, 0))
	wire.scale.z = global_transform.origin.distance_to(grapple.global_transform.origin)

func stand_up():
	var error = rad2deg(acos(transform.basis[1].dot(Vector3(0, 1, 0))))
	# No clue why Z had to be negative but X doesn't
	var error_vec = Vector2(-transform.basis[1].z, transform.basis[1].x).normalized() * error
	var x_result = stand_up_x_pid.iter(error_vec.x)
	var z_result = stand_up_z_pid.iter(error_vec.y)
	add_torque(Vector3(x_result, 0, z_result) * 0.2)

func _physics_process(delta):
	var horiz = Input.get_action_strength("ui_left") - Input.get_action_strength("ui_right")
	var vert = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	horiz *= 1.2
	vert *= 8
	add_torque(transform.basis[1] * horiz + transform.basis[0] * vert)
	stand_up()
	if grapple and grapple.is_anchored:
		var rel_vec = grapple.global_transform.origin - global_transform.origin
		var direction_to_grapple = rel_vec.normalized()
		var distance_to_grapple = rel_vec.length()
		# Would use grapple.linear_velocity but that stops getting set when it goes static
		var rel_vel = linear_velocity - Vector3(0, 0, 0)
		var wire_speed = rel_vel.dot(direction_to_grapple)
		print('Wire speed: ' + str(wire_speed))
		if (Input.is_action_pressed("pull_grapple") and
			wire_speed < grapple_top_speed and
			distance_to_grapple > 3
		):
			grapple_locked = true
			var accel = (grapple_top_speed - wire_speed) * grapple_accel
			add_central_force(direction_to_grapple * accel)
		if grapple_locked and wire_speed < 0:
			apply_central_impulse(direction_to_grapple * -wire_speed)
			print('Applied impulse of ' + str(direction_to_grapple * -wire_speed))
	else:
		grapple_locked = false
	point_wire_at_grapple()
