extends RigidBody

var is_anchored = false

func _ready():
	apply_central_impulse(global_transform.basis[2] * -60)
	set_contact_monitor(true)
	set_max_contacts_reported(1)

func _on_Grapple_body_entered(_body):
	set_mode(RigidBody.MODE_STATIC)
	is_anchored = true
